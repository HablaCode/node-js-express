const express = require('express');
const app = express();
const PORT = 9000;

// Middleware
// Viene en medio del ciclo de solicutud(req) y repuesta(res)
// Tiene accesso al objeto de request y response y tambien ala funcion de next
// Custom middleware create

const loggerMiddlware = (req, res, next) => {
  console.log(`Logged ${req.url} ${req.method} -- ${new Date()}`)
  next();
}

// Middleware de nivel de aplicacion
app.use(loggerMiddlware);

app.get('/', (req, res) => {
  res.send(console.log('Hola, soy una llamada GET de http'));
})

app.listen(PORT, () => {
  console.log(`Escuchando en port ${9000}`)
});